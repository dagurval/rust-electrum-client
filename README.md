# Network agnostic electrun client.

This is a fork of [electrum-client](https://crates.io/crates/electrum-client) crate with the dependency to `bitcoin` removed.

Network agnostic bitcoin Electrum client library. Supports plaintext, TLS and Onion servers.
