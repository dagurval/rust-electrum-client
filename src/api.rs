//! Electrum APIs

use batch::Batch;
use types::*;

/// API calls exposed by an Electrum client
pub trait ElectrumApi {
    /// Executes the requested API call returning the raw answer.
    fn raw_call(
        &self,
        method_name: &str,
        params: impl IntoIterator<Item = Param>,
    ) -> Result<serde_json::Value, Error>;

    /// Execute a queue of calls stored in a [`Batch`](../batch/struct.Batch.html) struct. Returns
    /// `Ok()` **only if** all of the calls are successful. The order of the JSON `Value`s returned
    /// reflects the order in which the calls were made on the `Batch` struct.
    fn batch_call(&self, batch: &Batch) -> Result<Vec<serde_json::Value>, Error>;

    /// Pings the server. This method can also be used as a "dummy" call to trigger the processing
    /// of incoming block header or script notifications.
    fn ping(&self) -> Result<(), Error>;

    #[cfg(feature = "debug-calls")]
    /// Returns the number of network calls made since the creation of the client.
    fn calls_made(&self) -> Result<usize, Error>;
}
