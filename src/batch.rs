//! Batch utilities
//!
//! This module contains definitions and helper functions used when making batch calls.

use types::{Call, Param};

/// Helper structure that caches all the requests before they are actually sent to the server.
///
/// Calls on this function are stored and run when [`batch_call`](../client/struct.Client.html#method.batch_call)
/// is run on a [`Client`](../client/struct.Client.html).
///
/// This structure can be used to make multiple *different* calls in one single run. For batch
/// calls of the same type, there are shorthands methods defined on the
/// [`Client`](../client/struct.Client.html), like
/// [`batch_script_get_balance`](../client/struct.Client.html#method.batch_script_get_balance) to ask the
/// server for the balance of multiple scripts with a single request.
#[derive(Default)]
pub struct Batch {
    calls: Vec<Call>,
}

impl Batch {
    /// Add a raw request to the batch queue
    pub fn raw(&mut self, method: String, params: Vec<Param>) {
        self.calls.push((method, params));
    }

    /// Returns an iterator on the batch
    pub fn iter(&self) -> BatchIter {
        BatchIter {
            batch: self,
            index: 0,
        }
    }
}

impl std::iter::IntoIterator for Batch {
    type Item = (String, Vec<Param>);
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.calls.into_iter()
    }
}

pub struct BatchIter<'a> {
    batch: &'a Batch,
    index: usize,
}

impl<'a> std::iter::Iterator for BatchIter<'a> {
    type Item = &'a (String, Vec<Param>);

    fn next(&mut self) -> Option<Self::Item> {
        let val = self.batch.calls.get(self.index);
        self.index += 1;
        val
    }
}


